package unit

import java.time.LocalDate

import controllers.WidgetForm
import org.scalatestplus.play.PlaySpec
import play.api.data.FormError
import play.api.i18n._
import play.api.mvc._
import play.api.test._

/**
 * Unit tests that do not require a running Play application.
 *
 * This is useful for testing forms and constraints.
 */
class UnitSpec extends PlaySpec {

  "WidgetForm" must {

    "apply successfully from request" in {
      // The easiest way to test a form is by passing it a fake request.
      val call = controllers.routes.WidgetController.createWidget()
      val content = "foo";
      val date = "2020-01-10"
      implicit val request: Request[_] = FakeRequest(call).withFormUrlEncodedBody("content" -> content, "date" ->  date)
      // A successful binding using an implicit request will give you a form with a value.
      val boundForm = WidgetForm.form.bindFromRequest()
      // You can then get the widget data out and test it.
      val widgetData = boundForm.value.get

      widgetData.content must equal(content)
      widgetData.date must equal(LocalDate.parse(date))
    }

    "apply successfully from map" in {
      // You can also bind directy from a map, if you don't have a request handy.
      val content = "foo";
      val date = "2020-01-10"
      val data = Map("content" -> content, "date" -> date)
      // A successful binding will give you a form with a value.
      val boundForm = WidgetForm.form.bind(data)
      // You can then get the widget data out and test it.
      val widgetData = boundForm.value.get

      widgetData.content must equal(content)
      widgetData.date must equal(LocalDate.parse(date))
    }

  }


}
