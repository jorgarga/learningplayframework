-- UserYearlyReminders schema

-- !Ups
ALTER TABLE UserYearlyReminders ADD COLUMN sendEmail INT;

-- !Downs
ALTER TABLE UserYearlyReminders DROP COLUMN sendEmail;
