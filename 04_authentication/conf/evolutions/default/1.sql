-- Users schema

-- !Ups

CREATE TABLE IF NOT EXISTS Users(
  id VARCHAR(50) PRIMARY KEY,
  name VARCHAR(255),
  email VARCHAR(255)
);


-- !Downs
DROP TABLE `Users`
