-- UserYearlyReminders schema

-- !Ups
CREATE TABLE IF NOT EXISTS UserYearlyReminders(
  userId VARCHAR(50),
  yearlyReminderId VARCHAR(50),
  created DATE,
  updated DATE,
  owner VARCHAR(50),
  editable INT,
  PRIMARY KEY (userId, yearlyReminderId),
  FOREIGN KEY (userId) REFERENCES Users(id),
  FOREIGN KEY (yearlyReminderId) REFERENCES YearlyReminders(id)
);

-- !Downs
DROP TABLE `UserYearlyReminders`
