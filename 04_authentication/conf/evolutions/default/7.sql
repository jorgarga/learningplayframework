-- UserYearlyReminders schema

-- !Ups
ALTER TABLE UserYearlyReminders ADD COLUMN sendSMS INT DEFAULT 0 NOT NULL

-- !Downs
ALTER TABLE UserYearlyReminders DROP COLUMN sendSMS;
