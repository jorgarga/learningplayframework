-- Users schema

-- !Ups
ALTER TABLE Users ADD COLUMN phonenumber VARCHAR(50); 

-- !Downs
ALTER TABLE Users DROP COLUMN phonenumber;
