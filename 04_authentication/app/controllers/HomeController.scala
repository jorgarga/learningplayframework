package controllers

import javax.inject._
import play.api.mvc._

/*
Code example from:
 https://alvinalexander.com/scala/play-framework-login-authentication-example-project/
 */

@Singleton
class HomeController @Inject()(cc: ControllerComponents) (implicit assetsFinder: AssetsFinder) extends AbstractController(cc) {

  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }
}
