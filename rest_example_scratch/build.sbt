name := """rest_example"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala, JavaAppPackaging, DockerPlugin, FlywayPlugin)

scalaVersion := "2.13.3"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.9.0"
libraryDependencies += "com.typesafe.play" %% "play-slick" % "5.0.0"
libraryDependencies += "org.postgresql" % "postgresql" % "9.4-1206-jdbc42"

flywayUrl := "jdbc:postgresql://localhost:5432/postgres?currentSchema=exercise"
flywayUser := "postgres"
flywayPassword := "password"
flywayLocations += "db/migration"

coverageExcludedPackages := "<empty>;Reverse.*;.*Routes.*"

javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)
