package controllers

import javax.inject.Inject
import models.JSONOrder
import play.api.Logger
import play.api.libs.json.Json._
import play.api.libs.json._
import play.api.mvc.{MessagesAbstractController, MessagesControllerComponents}
import services.OrderService

import scala.concurrent.ExecutionContext

class OrderController @Inject() (orderService: OrderService, cc: MessagesControllerComponents)
                                (implicit ec: ExecutionContext)
                                extends MessagesAbstractController(cc) {

  private val logger = Logger(getClass)

  def addOrder = Action.async { implicit request =>
    logger.debug(s"Controller addOrder: request = $request")
    val json = request.body.asJson.get
    val newOrder = json.as[JSONOrder]
    orderService.addOrder(newOrder).map { order =>
      logger.debug(s"Controller addOrder: order = $order")
      Ok(toJson(order))
    }
  }

  def getOrder(id: String) = Action.async { implicit request =>
    logger.debug(s"Controller getOrder: request = $request")
    orderService.getOrder(id).map { order =>
      Ok(Json.toJson(order))
    }
  }

  def getOrders = Action.async { implicit request =>
    logger.debug(s"Controller getOrders: request = $request")
    orderService.getOrders().map { orders =>
      Ok(Json.toJson(orders))
    }
  }

}

