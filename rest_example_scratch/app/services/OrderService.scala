package services

import javax.inject.Inject
import models.{JSONOrder, Order}
import play.api.Logger
import play.api.libs.json.Json
import repositories.OrderRepository

import scala.concurrent.Future

class OrderService @Inject()(repository: OrderRepository){
  private val logger = Logger(getClass)

  def addOrder(jsonOrder: JSONOrder) = {
    logger.debug(s"Service addOrder: jsonOrder = $jsonOrder")
    val jsonString = Json.toJson(jsonOrder.lineItems).toString()
    val newOrder = repository.create(jsonOrder.externalOrderId, jsonOrder.price, jsonString)
    newOrder
  }

  def getOrder(id: String): Future[Option[Order]] = {
    logger.debug(s"Service getOrder: id = $id")
    repository.get(id)
  }

  def getOrders() = {
    logger.debug(s"Service getOrders")
    repository.list()
  }

}
