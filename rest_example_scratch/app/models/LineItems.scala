package models

import play.api.libs.json.Json

case class LineItems(name: String, quantity: Int)

object LineItems {
  implicit val format = Json.format[LineItems]
}