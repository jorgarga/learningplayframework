package models

import play.api.libs.json.{JsResult, JsSuccess, JsValue, Json, Reads}

case class JSONOrder(externalOrderId: String, price: BigDecimal, lineItems: Seq[LineItems])

object JSONOrder {
  implicit val readsCreateOrderForm: Reads[JSONOrder] = new Reads[JSONOrder] {
    def reads(json: JsValue): JsResult[JSONOrder] = {
      val externalOrderId = (json \ "externalOrderId").as[String]
      val price = (json \ "price").as[BigDecimal]
      val items = (json \ "lineItems").as[Seq[LineItems]]
      JsSuccess(JSONOrder(externalOrderId, price, items))
    }
  }

  implicit val format = Json.format[LineItems]
}