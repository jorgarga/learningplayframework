package models

import play.api.libs.json._

case class Order(id: String, externalOrderId: String, price: BigDecimal, lineItems: String)

object Order {
  // "Writes" goes from model to JSON.
  implicit val writesOrder: Writes[Order] = new Writes[Order] {
    def writes(currentOrder: Order): JsValue = Json.obj(
      "id" -> currentOrder.id,
      "externalOrderId" -> currentOrder.externalOrderId,
      "price" -> currentOrder.price,
      "lineItems" -> Json.parse(currentOrder.lineItems)
    )
  }

}
