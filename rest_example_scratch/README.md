# Exercise
Implement a simple backend with 2 endpoints:

## Create order
This endpoint should receive order data together with a list of line items. 

### Request:
> POST /api/orders

```json
{
  "externalOrderId": "2000-20-0002",
  "price": 10.2,
  "lineItems": [
    {
      "name": "apple",
      "quantity": 20
    }
  ]
}
```

### Response:
```json
{
  "id": "a81bc81b-dead-4e5d-abff-90865d1e13b1"
  "externalOrderId": "2000-20-0002",
  "price": 10.2,
  "lineItems": [
    {
      "name": "apple",
      "quantity": 20
    }
  ]
}
```

## Read order by id
This endpoint should return order data for given id.

### Request:
> GET /api/orders/a81bc81b-dead-4e5d-abff-90865d1e13b1

### Response:
```json
{
  "id": "a81bc81b-dead-4e5d-abff-90865d1e13b1"
  "externalOrderId": "2000-20-0002",
  "price": 10.2,
  "lineItems": [
    {
      "name": "apple",
      "quantity": 20
    }
  ]
}
```

* Data is persisted in a PostgreSQL database.
* Communication with DB is done using Slick.
* Docker image built using [sbt-native-packager](https://www.scala-sbt.org/sbt-native-packager/formats/docker.html) plugin.

--------

# Working on the exercise
## 1.) Installing PostgreSQL using Docker

Pull an image with the latest stable release:
> docker pull postgres:12

Run the image with the following flags:
> docker run --detach --publish 5432:5432 --name my_postgres -e POSTGRES_PASSWORD=password --rm postgres:12
* `detach` sends the started container to the background.
* `publish` starts a port connection between host and container.
* `name` will give the container a name rather than a uuid/random string.
* `rm` will delete the container after it is run. 

**IMPORTANT**: Of course the password must be stronger in production environments. This is just a demo.

Connect to the container:
> docker exec -it my_postgres bash

Take a look to see that everything is OK:
> postgres=# \l
>
> postgres=# \q

Test the connection on the command line:
> psql -h localhost -p 5432 -U postgres


## 2.) The database SCHEMA

The [Flyway sbt](https://blog.knoldus.com/flyway-with-sbt/) integration takes care of database migrations after the SCHEMA is created.
The following sbt commands are available:

> sbt flywayClean

Deletes the database.

> sbt flywayMigrate

Starts a migration.

> sbt flywayInfo

Shows the flyway migrations table of the database.

To create a SCHEMA connect to the database either with the console or any database programm (like [dbeaver](https://dbeaver.io/))
and run the following:

```sql
CREATE SCHEMA exercise;
```

The migration scripts take over from this point and will run the following sql scripts:

```sql
CREATE EXTENSION IF NOT EXISTS "uuid-ossp"; # for the uuid function.
```

```sql
CREATE TABLE IF NOT EXISTS exercise.orders (
  id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
  external_order_id VARCHAR (50) UNIQUE NOT NULL,
  price NUMERIC,
  line_items json NOT NULL
);
```

The following command can be run to include some testing data:

```sql
INSERT INTO exercise.orders (external_order_id, price, line_items)
VALUES ('3000-33-0003', 13.5, '[{"product": "orange","quantity": 21}]');
```

## 3.) The implementation

### Architecture overview
```mermaid
graph LR
A{Controller} --> B[Service]
B --> C(Repository)
C --> D((Database))
```

### Application flow
```mermaid
sequenceDiagram
User ->> Controller: HTTP request/JSON
Controller -->> Service: Add / Retrieve the info
Service -->> Repository: Take a look in your list
Repository -->> Database: SQL query
Database -->> Repository: SQL result
Repository -->> Service: Send object back
Service -->> Controller: The result
Controller ->> User: HTTP answer/JSON.
```

## 4.) Testing
GET all orders
> curl --header "Content-Type: application/json" --request GET http://localhost:9000/orders

**NOTE**: Getting all orders is not part of the exercise, but it is good for testing.

Read one order
> curl --header "Content-Type: application/json" --request GET http://localhost:9000/orders/theIdOfTheOrder

Create new order
```json
curl --header "Content-Type: application/json" \
     --request POST \
     --data '{"externalOrderId": "2000-20-0002","price": 10.2, "lineItems": [{"name": "apple","quantity": 20}]}' \
     http://localhost:9000/orders
```

The following command will run the automatic tests:
> sbt test

The plugin `sbt-scoverage` has been installed, too. It provides following commands:
> sbt clean coverage test

to run the tests with enabled coverage.
To generate reports run:
> sbt coverageReport

The result will be found under:
> target/scala-2.13/scoverage-report/index.html

## 5.) Docker plugin
After the Docker plugin is installed the following sbt commands become available:

> docker:stage

Generates a directory with the Dockerfile and environment prepared for creating a Docker image.

> docker:publishLocal

Builds an image using the local Docker server.

> docker:publish

Builds an image using the local Docker server, and pushes it to the configured remote repository.

> docker:clean

Removes the built image from the local Docker server.

On can start a container from the created image with the following command:
> docker run --detach --publish 9000:9000 --name my_rest_example --rm --link=my_postgres:database rest_example:1.0-SNAPSHOT

* `link` connects my_postgress to the container under the name `database`.


## Putting it all together. How to run it and see it working.

Clone this repository (NOTE to self: maybe this folder should migrate to its own repo)
> git clone https://gitlab.com/jorgarga/learningplayframework.git
> cd learningplayframework/rest_example_scratch


Start the PostgreSQL container:
> docker run --detach --publish 5432:5432 --name my_postgres -e POSTGRES_PASSWORD=password --rm postgres:12


Create the SCHEMA:
> docker exec -it my_postgres bash
> psql -h localhost -p 5432 -U postgres
> CREATE SCHEMA exercise;
> exit
> exit


Apply the database migration:
> sbt flywayMigrate
> sbt flywayInfo


Create the project's docker image:
> sbt docker:publishLocal


Start the application's docker container:
> docker run --detach --publish 9000:9000 --name my_rest_example --rm --link=my_postgres:database rest_example:1.0-SNAPSHOT


Create a new order:
```json
curl --header "Content-Type: application/json" \
     --request POST \
     --data '{"externalOrderId": "2000-20-0002","price": 10.2, "lineItems": [{"name": "apple","quantity": 20}]}' \
     http://localhost:9000/orders
```


The response will look like this:
> {"id":"ef1abe1f-4b9b-4ce9-87c8-07c41cca2ac3","externalOrderId": "2000-20-0002","price": 10.2, "lineItems": [{"name": "apple","quantity": 20}]}


Fetch the newly created data:
> curl --header "Content-Type: application/json" --request GET http://localhost:9000/orders/ef1abe1f-4b9b-4ce9-87c8-07c41cca2ac3


# Conclusions
The exercise *per se* was easy, but the implementation process was **not**.
It was not easy mainly because of the lack of **full examples**.
Especially **frustrating** is the official Play framework documentation with dead links and only code snippets, leaving the newbie alone to figure out where to put that code. Some of the official examples do not even run out of the box on my machine :-(
There is a lot of documentation ([Slick](https://scala-slick.org/doc/3.3.1/), [Play Framework](https://www.playframework.com/documentation/2.8.x/Home), [Scala](https://docs.scala-lang.org/overviews/scala-book/introduction.html), etc), so I often get **the feeling that I am missing something**.

Apparently there are not so many people using the "Scala + Play framework" combination which means that is **difficult** to find help online. And the help one gets is in form of -yes- just code snippets or in best case small projects using deprecated content for an old version of Scala/Play framework.

I guess many (the majority?) of the developers have been using this technology for a long time and do not need as much context as new developers.

The community is **friendly** and welcomes newbies. I submitted some corrections to the official documentation which were accepted and merged to the mainstream branch.

After the exercise I understand a lot more. ¡There is nothing better than practicing to get experience!
Play framework is doing **a lot** of heavy work behind the scenes and the performance seen (requests/second, for example) are **very** impressive.

For this exercise I *could* have used the already-packed-code in one of the tutorials. But I wanted to make it from scratch and understand **every single line** of the code.