package models

import org.scalatest.Matchers._
import org.scalatest.FunSuite
import play.api.libs.json.{JsValue, Json}

class OrderTest extends FunSuite {

  val json: JsValue = Json.parse("""
  {
    "externalOrderId": "2000-20-0002",
    "price": 10.2,
    "lineItems": [
    {
      "name": "apple",
      "quantity": 20
    },
    {
      "name": "orange",
      "quantity": 25
    }
    ]
  }
  """)

  test("conversion from JSON to JSONOrder") {
    val newOrder = json.as[JSONOrder]
    newOrder.lineItems.size should be (2)
  }

  test("conversion from Order to JSON") {
    val expectedResult = "{\"id\":\"123\"," +
      "\"externalOrderId\":\"ext-123\"," +
      "\"price\":12.7," +
      "\"lineItems\":[{\"name\":\"apple\",\"quantity\":10},{\"name\":\"oranges\",\"quantity\":20}]}"

    val orderId = "123"
    val externalOrderId = "ext-123"
    val price = 12.7
    val lineItems = Json.toJson(Seq(LineItems("apple", 10), LineItems("oranges", 20))).toString()
    val order = Order(orderId, externalOrderId, price, lineItems)

    val json = Json.toJson(order)
    val jsonString = json.toString()

    json should not be null
    jsonString should equal (expectedResult)
  }

}
