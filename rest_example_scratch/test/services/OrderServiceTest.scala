package services

import models.{JSONOrder, LineItems, Order}
import org.scalatest.FunSuite
import play.api.libs.json.Json
import repositories.OrderRepository
import org.scalatest.Matchers._

import scala.concurrent.Future

class OrderServiceTest extends FunSuite {

  val newUUID = "blablabla"
  val orderId = "123"
  val externalOrderId = "ext-123"
  val price = 12.7
  val lineItems = "[{\"product\": \"orange\",\"quantity\": 21}, {\"product\": \"apple\",\"quantity\": 13}]"

  class RepositoryMock extends OrderRepository {
    var orders = Map(
      orderId -> Order(orderId, externalOrderId, price, lineItems),
      "234" -> Order("234", "ext-234", 13, "[{\"product\": \"orange\",\"quantity\": 21}]"),
      "345" -> Order("354", "ext-345", 15, "[{\"product\": \"orange\",\"quantity\": 21}, {\"product\": \"apple\",\"quantity\": 13}]")
    )

    override def create(externalOrderId: String, price: BigDecimal, lineItems: String): Future[Order] = {
      val order = Order(newUUID, externalOrderId, price, lineItems)
      orders += (newUUID -> order)
      Future.successful(order)
    }

    override def get(id: String): Future[Option[Order]] = {
      Future.successful(orders.get(id))
    }

    override def list(): Future[Seq[Order]] = {
      Future.successful(
        orders.values.toSeq
      )
    }
  }

  val repository = new RepositoryMock()
  val orderService = new OrderService(repository)

  test("testGetOrder") {
    val order = orderService.getOrder(orderId).value.get.get.get // Really???
    assert(order.id == orderId)
  }

  test("testAddOrder") {
    val jsonOrder = JSONOrder("external-Id", 13.4, Seq(LineItems("apple", 3)))
    val newOrder = orderService.addOrder(jsonOrder)
    assert(newOrder.value.get.get.id == newUUID)
  }

  test("testGetOrders") {
    val orders = orderService.getOrders().value.get.get
    orders.size should be >= 3
  }

}
