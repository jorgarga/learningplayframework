CREATE TABLE IF NOT EXISTS exercise.orders (
	id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
	external_order_id VARCHAR (50) UNIQUE NOT NULL,
    price NUMERIC,
    line_items TEXT NOT NULL
);

