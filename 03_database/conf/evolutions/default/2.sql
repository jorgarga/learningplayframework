-- YearlyReminders schema

-- !Ups

CREATE TABLE IF NOT EXISTS YearlyReminders(
  id VARCHAR(50) PRIMARY KEY,
  content VARCHAR(255),
  date DATE
);

-- !Downs
DROP TABLE `YearlyReminders`
