-- Users schema

-- !Ups
ALTER TABLE Users ADD COLUMN password VARCHAR(255); 

-- !Downs
ALTER TABLE Users DROP COLUMN password;
