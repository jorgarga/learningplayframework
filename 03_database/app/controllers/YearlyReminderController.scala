package controllers

import javax.inject._
import models.{YearlyReminder, YearlyReminderForm}
import play.api.Logging
import play.api.mvc._
import services.YearlyReminderService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import io.jvm.uuid._

@Singleton
class YearlyReminderController @Inject()(cc: ControllerComponents, yearlyReminderService: YearlyReminderService)
                                        (implicit assetsFinder: AssetsFinder) extends AbstractController(cc) with Logging {
  def index() = Action.async { implicit request: Request[AnyContent] =>
    yearlyReminderService.listAll map { yearlyReminders =>
      Ok(views.html.yearlyReminder(YearlyReminderForm.form, yearlyReminders))
    }
  }

  def addYearlyReminder() = Action.async { implicit request: Request[AnyContent] =>
    YearlyReminderForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => {
        logger.warn(s"Form submission with error: ${errorForm.errors}")
        Future.successful(Ok(views.html.yearlyReminder(errorForm, Seq.empty[YearlyReminder])))
      },
      data => {
        val newYearlyReminder = YearlyReminder(UUID.random.toString, data.content, data.date)
        yearlyReminderService.add(newYearlyReminder).map( _ => Redirect(routes.YearlyReminderController.index()))
      })
  }

  def deleteYearlyReminder(id: String) = Action.async { implicit request: Request[AnyContent] =>
    yearlyReminderService.delete(id) map { res =>
      Redirect(routes.YearlyReminderController.index())
    }
  }
}
