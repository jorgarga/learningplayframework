package models

import com.google.inject.Inject
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.MySQLProfile.api._

case class UserYearlyReminder(userId: String,
                              yearlyReminderId: String,
                              created: String,
                              updated: String,
                              owner: String,
                              editable: Int,
                              sendEmail: Int,
                              sendSMS: Int)

class UserYearlyRemindersTable(tag: Tag) extends Table[UserYearlyReminder](tag, "UserYearlyReminders") {

  def userId = column[String]("userId")
  def yearlyReminderId = column[String]("yearlyReminderId")

  def created = column[String]("created")
  def updated = column[String]("updated")
  def owner = column[String]("owner")
  def editable = column[Int]("editable")
  def sendEmail = column[Int]("sendEmail")
  def sendSMS = column[Int]("sendSMS")

  override def * = (userId, yearlyReminderId,
    created, updated, owner, editable, sendEmail, sendSMS) <>(UserYearlyReminder.tupled, UserYearlyReminder.unapply)

  def userFK = foreignKey("FK_Users",
    userId,
    TableQuery[UserTable])( user => userId, onDelete=ForeignKeyAction.Cascade)

  def yearlyReminderFK = foreignKey("FK_YearlyReminders",
    yearlyReminderId,
    TableQuery[YearlyReminderTable]) ( yearlyReminder => yearlyReminderId, onDelete=ForeignKeyAction.Cascade
  )
}
/*
class YearlyReminders @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit executionContext: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] {

  val yearlyReminders = TableQuery[YearlyReminderTable]

  def add(reminder: YearlyReminder): Future[String] = {
    dbConfig.db.run(yearlyReminders += reminder).map(res => "YearlyReminder successfully added").recover {
      case ex: Exception => ex.getCause.getMessage
    }
  }

  def delete(id: String): Future[Int] = {
    dbConfig.db.run(yearlyReminders.filter(_.id === id).delete)
  }

  def get(id: String): Future[Option[YearlyReminder]] = {
    dbConfig.db.run(yearlyReminders.filter(_.id === id).result.headOption)
  }

  def listAll: Future[Seq[YearlyReminder]] = {
    dbConfig.db.run(yearlyReminders.result)
  }

}
*/