package models

import com.google.inject.Inject
import io.jvm.uuid.UUID
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

case class User(id: String, name: String, email: String, password: String, phonenumber: String)

case class UserFormData(name: String, email: String, password: String, phonenumber: String)

object UserForm {
  val form = Form(
    mapping(
      "name" -> nonEmptyText,
      "email" -> nonEmptyText,
      "password" -> nonEmptyText,
      "phonenumber" -> text
    )(UserFormData.apply)(UserFormData.unapply)
  )
}

import slick.jdbc.MySQLProfile.api._

class UserTable(tag: Tag) extends Table[User](tag, "Users") {

  def id = column[String]("id", O.PrimaryKey)
  def name = column[String]("name")
  def email = column[String]("email")
  def password = column[String]("password")
  def phonenumber = column[String]("phonenumber")

  override def * =
    (id, name, email, password, phonenumber) <>(User.tupled, User.unapply)
}

class Users @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit executionContext: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] {

  val users = TableQuery[UserTable]

  def add(user: User): Future[String] = {
      dbConfig.db.run(users += user).map(res => "User successfully added").recover {
        case ex: Exception => ex.getCause.getMessage
      }
  }
  def register(userFormData: UserFormData): Future[String] = {
      // TODO: hash password
      val user = User(UUID.random.toString, userFormData.name, userFormData.email, userFormData.password, userFormData.phonenumber)
      dbConfig.db.run(users += user).map(res => "User successfully added").recover {
        case ex: Exception => ex.getCause.getMessage
      }
  }

  def delete(id: String): Future[Int] = {
    dbConfig.db.run(users.filter(_.id === id).delete)
  }

  def getById(id: String): Future[Option[User]] = {
    dbConfig.db.run(users.filter(_.id === id).result.headOption)
  }

  def getByEmail(email: String): Future[Option[User]] = {
    dbConfig.db.run(users.filter(_.email === email).result.headOption)
  }

  def listAll: Future[Seq[User]] = {
    dbConfig.db.run(users.result)
  }

}