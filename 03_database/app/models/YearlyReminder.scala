package models

import com.google.inject.Inject
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.MySQLProfile.api._

case class YearlyReminder(id: String, content: String, date: String)

case class YearlyReminderFormData(content: String, date: String)

object YearlyReminderForm {
  val form = Form(
  mapping(
  "content" -> text(minLength=3, maxLength = 140),
  "date" -> nonEmptyText
  )(YearlyReminderFormData.apply)(YearlyReminderFormData.unapply)
  )
}

class YearlyReminderTable(tag: Tag) extends Table[YearlyReminder](tag, "YearlyReminders") {

  def id = column[String]("id", O.PrimaryKey)
  def content = column[String]("content")
  def date = column[String]("date")

  override def * =
    (id, content, date) <>(YearlyReminder.tupled, YearlyReminder.unapply)
}

class YearlyReminders @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit executionContext: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] {

  val yearlyReminders = TableQuery[YearlyReminderTable]

  def add(reminder: YearlyReminder): Future[String] = {
    dbConfig.db.run(yearlyReminders += reminder).map(res => "YearlyReminder successfully added").recover {
      case ex: Exception => ex.getCause.getMessage
    }
  }

  def delete(id: String): Future[Int] = {
    dbConfig.db.run(yearlyReminders.filter(_.id === id).delete)
  }

  def get(id: String): Future[Option[YearlyReminder]] = {
    dbConfig.db.run(yearlyReminders.filter(_.id === id).result.headOption)
  }

  def listAll: Future[Seq[YearlyReminder]] = {
    dbConfig.db.run(yearlyReminders.result)
  }

}
