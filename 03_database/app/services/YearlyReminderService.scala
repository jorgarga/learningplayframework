package services

import com.google.inject.Inject
import models.{YearlyReminder, YearlyReminders}

import scala.concurrent.Future

class YearlyReminderService @Inject() (yearlyReminders: YearlyReminders) {

  def add(yearlyReminder: YearlyReminder): Future[String] = {
    yearlyReminders.add(yearlyReminder)
  }

  def delete(id: String): Future[Int] = {
    yearlyReminders.delete(id)
  }

  def get(id: String): Future[Option[YearlyReminder]] = {
    yearlyReminders.get(id)
  }

  def listAll: Future[Seq[YearlyReminder]] = {
    yearlyReminders.listAll
  }
}
