lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := """03_database""",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.13.1",
    libraryDependencies ++= Seq(
      guice,
      "com.typesafe.play" %% "play-slick" % "5.0.0",
      "com.typesafe.play" %% "play-slick-evolutions" % "5.0.0",
      "io.jvm.uuid" %% "scala-uuid" % "0.3.1",
      "mysql" % "mysql-connector-java" % "8.0.15",
      "org.xerial" % "sqlite-jdbc" % "3.31.1",
      "com.h2database" % "h2" % "1.4.199",
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
    ),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    )
  )
