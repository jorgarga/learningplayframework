package controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 *
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class HomeControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "HomeController GET" should {

    "render the index page from a new instance of controller" in {
      val controller = new HomeController(stubControllerComponents())
      val home = controller.index().apply(FakeRequest(GET, "/"))

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to Play")
    }

    "render the index page from the application" in {
      val controller = inject[HomeController]
      val home = controller.index().apply(FakeRequest(GET, "/"))

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to Play")
    }

    "render the index page from the router" in {
      val request = FakeRequest(GET, "/")
      val home = route(app, request).get

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to Play")
    }

    "render the hello page from a new instance of controller" in {
      val controller = new HomeController(stubControllerComponents())
      val hello = controller.hello("World").apply(FakeRequest(GET, "/hello"))

      status(hello) mustBe OK
      contentType(hello) mustBe Some("text/html")
      contentAsString(hello) must include ("Hello, World")
    }

    "render the hello page from the application" in {
      val controller = inject[HomeController]
      val hello = controller.hello("World").apply(FakeRequest(GET, "/hello"))

      status(hello) mustBe OK
      contentType(hello) mustBe Some("text/html")
      contentAsString(hello) must include ("Hello, World")
    }

    "render the hello page from the router" in {
      val request = FakeRequest(GET, "/hello?name=World")
      val helloWorld = route(app, request).get

      status(helloWorld) mustBe OK
      contentType(helloWorld) mustBe Some("text/html")
      contentAsString(helloWorld) must include ("Hello, World")
    }
  }
}
