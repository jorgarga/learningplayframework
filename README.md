# Learning Play framework
This is a summury of my learning process.


Play framework uses [sbt](https://www.scala-sbt.org/1.x/docs/Hello.html) to run, compile, buid, deploy, etc. To run applications use the following command:
> sbt run

Debugging the app.

 1. Run the application with the following command `sbt run -jvm-debug 9999`
 2. In IntelliJIDEA create a Remote configuration and select `9999` as port.

# Applications
## Hello World
In this simple MVC page one can talk to 2 controllers:

 - index which will render "Welcome to Play!"
 - hello which will render the text given as parameter (eg. http://localhost:9000/hello?name=World)
 

## Forms
In this example one can see how to work with forms.


## Database
One MySQL connection is established to a server and info can be saved/deleted from the database.


## Authentication
Home made example of authentication. The code is copied from [alvinalexander](https://alvinalexander.com),
[How to implement user authentication in a Play Framework application](https://alvinalexander.com/scala/how-to-implement-user-authentication-play-framework-application/).

# Interesting Info

* [Creating a REST API](https://developer.lightbend.com/guides/play-rest-api/part-1/index.html)